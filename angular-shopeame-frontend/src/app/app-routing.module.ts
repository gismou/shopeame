import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', loadChildren: () =>
    import('./pages/home-page/home-page.module').then(m => m.HomePageModule)  
  },
  {
    path: 'management', loadChildren: () =>
    import('./pages/management-page/management-page.module').then(m => m.ManagementPageModule)  
  },
  {
    path: 'products', loadChildren: () =>
    import('./pages/products-page/products-page.module').then(m => m.ProductsPageModule)  
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
